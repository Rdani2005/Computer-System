package clases;

import java.sql.*;

public class Conection {
    // Local dev Conection
    public static Connection conect() {
        try {
            Connection cn = DriverManager.getConnection(
                    "jdbc:mysql://localhost/bd_ds",
                    "root",
                    ""
            );
            return cn;
        
        } catch (SQLException e) {
            System.out.println("Local Conection Error!!! " + e );
        }    
        return (null);
    }
}
