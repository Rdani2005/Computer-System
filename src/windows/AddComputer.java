package windows;

import clases.Conection;
import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.Image;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

public class AddComputer extends javax.swing.JFrame {

    String user, clientUpdate;
    int user_id;

    public AddComputer() {
        initComponents();
        setSize(630, 445);
        setResizable(false);
        setLocationRelativeTo(null);

        // Background image configuration
        ImageIcon wallpaper = new ImageIcon("src/images/wallpaperPrincipal.jpg");
        Icon icon = new ImageIcon(
                wallpaper.getImage().getScaledInstance(
                        BackgroundLbl.getWidth(),
                        BackgroundLbl.getHeight(),
                        Image.SCALE_DEFAULT
                )
        );
        BackgroundLbl.setIcon(icon);
        this.repaint();

        // Logical configs
        clientUpdate = ManageClients.clientUpdate;
        user = Login.user;
        user_id = ManageClients.Id_Client_Update;
        setTitle("Register computer from: " + clientUpdate);
        // We don´t need the program to end
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        DamageTxt.setLineWrap(true);

        NameTxt.setText(clientUpdate);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TitleLbl = new javax.swing.JLabel();
        ClientName = new javax.swing.JLabel();
        NameTxt = new javax.swing.JTextField();
        BrandLbl = new javax.swing.JLabel();
        ModelTxt = new javax.swing.JTextField();
        TypeLbl = new javax.swing.JLabel();
        ComputerTypeCmb = new javax.swing.JComboBox();
        BrandCmb = new javax.swing.JComboBox();
        ModelLbl = new javax.swing.JLabel();
        SerialLbl = new javax.swing.JLabel();
        SerialTxt = new javax.swing.JTextField();
        DamgeLbl = new javax.swing.JLabel();
        DamageScroll = new javax.swing.JScrollPane();
        DamageTxt = new javax.swing.JTextArea();
        RegisterButton = new javax.swing.JButton();
        CopyrightLbl = new javax.swing.JLabel();
        BackgroundLbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        TitleLbl.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        TitleLbl.setForeground(new java.awt.Color(255, 255, 255));
        TitleLbl.setText("Computer Register");
        getContentPane().add(TitleLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, -1, -1));

        ClientName.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        ClientName.setForeground(new java.awt.Color(255, 255, 255));
        ClientName.setText("Client Name:");
        getContentPane().add(ClientName, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, -1, -1));

        NameTxt.setEditable(false);
        NameTxt.setBackground(new java.awt.Color(153, 153, 255));
        NameTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        NameTxt.setForeground(new java.awt.Color(255, 255, 255));
        NameTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        NameTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(NameTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 210, -1));

        BrandLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        BrandLbl.setForeground(new java.awt.Color(255, 255, 255));
        BrandLbl.setText("Brand:");
        getContentPane().add(BrandLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, -1, -1));

        ModelTxt.setBackground(new java.awt.Color(153, 153, 255));
        ModelTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        ModelTxt.setForeground(new java.awt.Color(255, 255, 255));
        ModelTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        ModelTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(ModelTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 250, 210, -1));

        TypeLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        TypeLbl.setForeground(new java.awt.Color(255, 255, 255));
        TypeLbl.setText("Type:");
        getContentPane().add(TypeLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, -1, -1));

        ComputerTypeCmb.setBackground(new java.awt.Color(153, 153, 255));
        ComputerTypeCmb.setForeground(new java.awt.Color(255, 255, 255));
        ComputerTypeCmb.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "laptop", "desktop", "all in one" }));
        getContentPane().add(ComputerTypeCmb, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, 210, -1));

        BrandCmb.setBackground(new java.awt.Color(153, 153, 255));
        BrandCmb.setForeground(new java.awt.Color(255, 255, 255));
        BrandCmb.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "acer", "apple", "hp", "toshiba", "dell" }));
        getContentPane().add(BrandCmb, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 210, -1));

        ModelLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        ModelLbl.setForeground(new java.awt.Color(255, 255, 255));
        ModelLbl.setText("Model:");
        getContentPane().add(ModelLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 230, -1, -1));

        SerialLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        SerialLbl.setForeground(new java.awt.Color(255, 255, 255));
        SerialLbl.setText("Serial Number:");
        getContentPane().add(SerialLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 290, -1, -1));

        SerialTxt.setBackground(new java.awt.Color(153, 153, 255));
        SerialTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        SerialTxt.setForeground(new java.awt.Color(255, 255, 255));
        SerialTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        SerialTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(SerialTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 310, 210, -1));

        DamgeLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        DamgeLbl.setForeground(new java.awt.Color(255, 255, 255));
        DamgeLbl.setText("Damage Report:");
        getContentPane().add(DamgeLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 50, -1, -1));

        DamageScroll.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        DamageScroll.setToolTipText("");

        DamageTxt.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        DamageTxt.setRows(5);
        DamageScroll.setViewportView(DamageTxt);

        getContentPane().add(DamageScroll, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 70, 295, 260));

        RegisterButton.setBackground(new java.awt.Color(153, 153, 255));
        RegisterButton.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        RegisterButton.setForeground(new java.awt.Color(255, 255, 255));
        RegisterButton.setText("Register");
        RegisterButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RegisterButtonActionPerformed(evt);
            }
        });
        getContentPane().add(RegisterButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(465, 340, 147, -1));

        CopyrightLbl.setText("Copyright © Danny Sequeira");
        getContentPane().add(CopyrightLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 400, -1, -1));
        getContentPane().add(BackgroundLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 630, 445));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void RegisterButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RegisterButtonActionPerformed
        AddComputer();
    }//GEN-LAST:event_RegisterButtonActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            new AddComputer().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel BackgroundLbl;
    private javax.swing.JComboBox BrandCmb;
    private javax.swing.JLabel BrandLbl;
    private javax.swing.JLabel ClientName;
    private javax.swing.JComboBox ComputerTypeCmb;
    private javax.swing.JLabel CopyrightLbl;
    private javax.swing.JScrollPane DamageScroll;
    private javax.swing.JTextArea DamageTxt;
    private javax.swing.JLabel DamgeLbl;
    private javax.swing.JLabel ModelLbl;
    private javax.swing.JTextField ModelTxt;
    private javax.swing.JTextField NameTxt;
    private javax.swing.JButton RegisterButton;
    private javax.swing.JLabel SerialLbl;
    private javax.swing.JTextField SerialTxt;
    private javax.swing.JLabel TitleLbl;
    private javax.swing.JLabel TypeLbl;
    // End of variables declaration//GEN-END:variables

    private void AddComputer() {
        String type_string, brand_string, model, serial_number, damage;
        int validator = 0;
        // Getting the data from the text boxes and text areas.
        model = ModelTxt.getText().trim();
        serial_number = SerialTxt.getText().trim();
        damage = DamageTxt.getText().trim();

        // Validating there are not empty spaces
        if (model.equals("")) {
            ModelTxt.setBackground(Color.red);
            validator++;
        }

        if (serial_number.equals("")) {
            SerialTxt.setBackground(Color.red);
            validator++;
        }

        if (damage.equals("")) {
            DamageTxt.setBackground(Color.red);
            validator++;
        }

        // Let's add the data to the database!
        if (validator == 0) {

            // Setting a time variable
            // We need the computers brands and types
            // Computer type
            type_string = ComputerTypeCmb.getSelectedItem().toString();
            // Computer brand
            brand_string = BrandCmb.getSelectedItem().toString();
            try {
                Connection cn2 = Conection.conect();
                PreparedStatement pst2 = cn2.prepareStatement(
                        "insert into computers values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
                );
                // Adding data to the database
                pst2.setInt(1, 0);
                pst2.setInt(2, user_id);
                pst2.setString(3, type_string);
                pst2.setString(4, brand_string);
                pst2.setString(5, model);
                pst2.setString(6, serial_number);
                pst2.setString(7, "" + LocalDate.now().getDayOfMonth());
                pst2.setString(8, "" + LocalDate.now().getMonth());
                pst2.setString(9, "" + LocalDate.now().getYear());
                pst2.setString(10, damage);
                pst2.setString(11, "etering");
                pst2.setString(12, "" + LocalDate.now());
                pst2.setString(13, "");
                pst2.setString(14, user);

                pst2.executeUpdate();
                cn2.close();

                cleanText();

                ModelTxt.setBackground(Color.GREEN);
                NameTxt.setBackground(Color.GREEN);
                SerialTxt.setBackground(Color.GREEN);
                DamageTxt.setBackground(Color.GREEN);

                JOptionPane.showMessageDialog(null, "Computer added successfully!");

                this.dispose();

            } catch (SQLException | HeadlessException e) {
                System.err.println("Error adding the computer! " + e);
                JOptionPane.showMessageDialog(null, "Database Error!!!");
            }

        } else {
            JOptionPane.showMessageDialog(null, "There are empty spaces!");
        }
    }
    
    private void cleanText() {
        ModelTxt.setText("");
        DamageTxt.setText("");
        SerialTxt.setText("");
    }
}
