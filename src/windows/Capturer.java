package windows;

import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.WindowConstants;
// MySQL Connectors
import java.sql.*;
import clases.Conection;
// Itext Libraries
import com.itextpdf.text.Font;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.DocumentException;

public class Capturer extends javax.swing.JFrame {

    String user, name;
    int UserSession;
    
    public Capturer() {
        initComponents();
        
        user = Login.user;
        UserSession = Administrator.user_session;
        
        setTitle("Capturer Window - Session Owner: " + user);
        setLocationRelativeTo(null);
        
        setSize(550, 300);
        setResizable(false);
        // Background image configuration
        ImageIcon wallpaper = new ImageIcon("src/images/wallpaperPrincipal.jpg");
        Icon icon = new ImageIcon(
                wallpaper.getImage().getScaledInstance(
                        BackgroundLbl.getWidth(),
                        BackgroundLbl.getHeight(),
                        Image.SCALE_DEFAULT
                )
        );
        BackgroundLbl.setIcon(icon);
        this.repaint();
        
        if (UserSession == 1) {
            setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        } else {
            setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        }
        
        try {
            Connection cn = Conection.conect();
            PreparedStatement pst = cn.prepareStatement(
                    "select name from users where username = '" + user + "'"
            );
            
            ResultSet rs = pst.executeQuery();
            
            if (rs.next()) {
               name = rs.getString("name");
               NameLbl.setText("Welcome " + name);
            }
            
        } catch (SQLException e) {
            System.err.println("Error getting the user Data! " + e);
        }
    }
    
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/icon.png"));
        return retValue;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        NameLbl = new javax.swing.JLabel();
        RegisterClientButton = new javax.swing.JButton();
        RegisterClientLbl = new javax.swing.JLabel();
        ManageClientsButon = new javax.swing.JButton();
        MngeClientsLbl = new javax.swing.JLabel();
        PrintButton = new javax.swing.JButton();
        PrintLbl = new javax.swing.JLabel();
        CopyrightLbl = new javax.swing.JLabel();
        BackgroundLbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getIconImage());
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        NameLbl.setFont(new java.awt.Font("Arial", 1, 20)); // NOI18N
        NameLbl.setForeground(new java.awt.Color(255, 255, 255));
        getContentPane().add(NameLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        RegisterClientButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/add.png"))); // NOI18N
        RegisterClientButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RegisterClientButtonActionPerformed(evt);
            }
        });
        getContentPane().add(RegisterClientButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 80, 120, 120));

        RegisterClientLbl.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        RegisterClientLbl.setForeground(new java.awt.Color(255, 255, 255));
        RegisterClientLbl.setText("Register Client");
        getContentPane().add(RegisterClientLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 200, -1, -1));

        ManageClientsButon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/informationuser.png"))); // NOI18N
        ManageClientsButon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ManageClientsButonActionPerformed(evt);
            }
        });
        getContentPane().add(ManageClientsButon, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 80, 120, 120));

        MngeClientsLbl.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        MngeClientsLbl.setForeground(new java.awt.Color(255, 255, 255));
        MngeClientsLbl.setText("Manage Clients");
        getContentPane().add(MngeClientsLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 200, -1, -1));

        PrintButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/impresora.png"))); // NOI18N
        getContentPane().add(PrintButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 80, 120, 120));

        PrintLbl.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        PrintLbl.setForeground(new java.awt.Color(255, 255, 255));
        PrintLbl.setText("Print Clients");
        getContentPane().add(PrintLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 200, -1, -1));

        CopyrightLbl.setText("Copyright © Danny Sequeira");
        getContentPane().add(CopyrightLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 240, -1, -1));
        getContentPane().add(BackgroundLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 550, 300));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void RegisterClientButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RegisterClientButtonActionPerformed
        new RegisterClients().setVisible(true);
    }//GEN-LAST:event_RegisterClientButtonActionPerformed

    private void ManageClientsButonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ManageClientsButonActionPerformed
        new ManageClients().setVisible(true);
    }//GEN-LAST:event_ManageClientsButonActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            new Capturer().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel BackgroundLbl;
    private javax.swing.JLabel CopyrightLbl;
    private javax.swing.JButton ManageClientsButon;
    private javax.swing.JLabel MngeClientsLbl;
    private javax.swing.JLabel NameLbl;
    private javax.swing.JButton PrintButton;
    private javax.swing.JLabel PrintLbl;
    private javax.swing.JButton RegisterClientButton;
    private javax.swing.JLabel RegisterClientLbl;
    // End of variables declaration//GEN-END:variables
}
