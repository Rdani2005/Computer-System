package windows;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;

import clases.Conection;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

public class ManageClients extends javax.swing.JFrame {

    String user;
    public static String clientUpdate = "";
    public static int Id_Client_Update = 0;
    DefaultTableModel model = new DefaultTableModel();
    
    public ManageClients() {
        initComponents();
        user = Login.user;
        setTitle("Registered Clients - session owner: " + user);
        setSize(630, 330);
        setResizable(false);
        setLocationRelativeTo(null);
        // We don´t need the program to end
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE); 
        
        // Background image configuration
        ImageIcon wallpaper = new ImageIcon("src/images/wallpaperPrincipal.jpg");
        Icon icon = new ImageIcon(
                wallpaper.getImage().getScaledInstance(
                        BackgroundLbl.getWidth(),
                        BackgroundLbl.getHeight(),
                        Image.SCALE_DEFAULT
                )
        );
        BackgroundLbl.setIcon(icon);
        this.repaint();
        
        try {
            
            Connection cn = Conection.conect();
            PreparedStatement pst = cn.prepareStatement(
                    "select id, name, email, phone, last_modified from clients"
            );
            
            ResultSet rs = pst.executeQuery();

            ClientsTable = new JTable(model);
            ClientsTableScroll.setViewportView(ClientsTable);
            
            model.addColumn(" ");
            model.addColumn("name");
            model.addColumn("email");
            model.addColumn("phone");
            model.addColumn("Modified By");

            
            while(rs.next()) {
                Object[] row = new Object[5];
                
                for (int i = 0; i < 5; i++) {
                    row[i] = rs.getObject(i + 1);
                }
                model.addRow(row);   
            }
            cn.close();
            
        } catch (SQLException e) {
            System.err.print("Error conecting to the DB on Manage Clients Form!!! " + e);
            JOptionPane.showMessageDialog(null, "Error! Contact the admin for more information");
        }
        
            ClientsTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                int row_pointer = ClientsTable.rowAtPoint(evt.getPoint());
                int column_pointer = 1;
                
                if (row_pointer > -1) {
                    clientUpdate = (String) model.getValueAt(row_pointer, column_pointer);
                    Id_Client_Update = (int) model.getValueAt(row_pointer, 0);
                    new ModifyClient().setVisible(true);
                }
            }
        
        });
    }
    
    
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/informationuser.png"));
        return retValue;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ClientsTableScroll = new javax.swing.JScrollPane();
        ClientsTable = new javax.swing.JTable();
        TitleLbl = new javax.swing.JLabel();
        CopyrightLbl = new javax.swing.JLabel();
        BackgroundLbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getIconImage());
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        ClientsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        ClientsTableScroll.setViewportView(ClientsTable);

        getContentPane().add(ClientsTableScroll, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 70, 630, 180));

        TitleLbl.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        TitleLbl.setForeground(new java.awt.Color(255, 255, 255));
        TitleLbl.setText("Clients on Database");
        getContentPane().add(TitleLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 10, -1, -1));

        CopyrightLbl.setText("Copyright © Danny Sequeira");
        getContentPane().add(CopyrightLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 280, -1, -1));

        BackgroundLbl.setText("jLabel1");
        getContentPane().add(BackgroundLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 630, 330));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            new ManageClients().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel BackgroundLbl;
    private javax.swing.JTable ClientsTable;
    private javax.swing.JScrollPane ClientsTableScroll;
    private javax.swing.JLabel CopyrightLbl;
    private javax.swing.JLabel TitleLbl;
    // End of variables declaration//GEN-END:variables
}
