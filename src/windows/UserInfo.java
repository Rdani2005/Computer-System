package windows;

import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.WindowConstants;

import java.sql.*;
import clases.Conection;
import java.awt.Color;
import javax.swing.JOptionPane;

public class UserInfo extends javax.swing.JFrame {
    
    String user, userUpdate;
    int ID;
    
    
    public UserInfo() {
        initComponents(); 
        user = Login.user;
        userUpdate = ManageUsers.userUpdate;
        
        // Window configs
        setSize(630, 450);
        setResizable(false);
        setLocationRelativeTo(null);
        setTitle("User information - Session Owner: " + user);
        
        // We don´t need the program to end
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE); 
        
        // Background image configuration
        ImageIcon wallpaper = new ImageIcon("src/images/wallpaperPrincipal.jpg");
        Icon icon = new ImageIcon(
                wallpaper.getImage().getScaledInstance(
                        BackgroundLbl.getWidth(),
                        BackgroundLbl.getHeight(),
                        Image.SCALE_DEFAULT
                )
        );
        BackgroundLbl.setIcon(icon);
        this.repaint();
        
        TitleLbl.setText("User Information: " + userUpdate);
        
        try {
            Connection cn = Conection.conect();
            
            PreparedStatement pst = cn.prepareStatement(
                    "select * from users where username = '" + userUpdate + "'"
            );
            
            ResultSet rs = pst.executeQuery();
            
            if (rs.next()) {
                ID = rs.getInt("id");
                
                NameTxt.setText(rs.getString("name"));
                EmailTxt.setText(rs.getString("email"));
                UsernameTxt.setText(rs.getString("username"));
                RegisterByTxt.setText(rs.getString("register_by"));
                PhoneTxt.setText(rs.getString("phone"));
                LevelsCmb.setSelectedItem(rs.getString("level"));
                StatusCmb.setSelectedItem(rs.getString("status"));
            }
            cn.close();
        } catch (SQLException e) {
            System.err.print("Error while connecting to the database on to fullfill the textboxs!! " + e);
            JOptionPane.showMessageDialog(null, "Error! Contact the administrator");
        }
    }
    
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/informationuser.png"));
        return retValue;
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TitleLbl = new javax.swing.JLabel();
        NameLbl = new javax.swing.JLabel();
        UsernameLbl = new javax.swing.JLabel();
        EmailLbl = new javax.swing.JLabel();
        PhoneLbl = new javax.swing.JLabel();
        LevelLbl = new javax.swing.JLabel();
        StatusLbl = new javax.swing.JLabel();
        RegisterByLbl = new javax.swing.JLabel();
        NameTxt = new javax.swing.JTextField();
        UsernameTxt = new javax.swing.JTextField();
        EmailTxt = new javax.swing.JTextField();
        PhoneTxt = new javax.swing.JTextField();
        RegisterByTxt = new javax.swing.JTextField();
        LevelsCmb = new javax.swing.JComboBox();
        StatusCmb = new javax.swing.JComboBox();
        UpdateButton = new javax.swing.JButton();
        RestartPassLbl = new javax.swing.JButton();
        CopyrightLbl = new javax.swing.JLabel();
        BackgroundLbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getIconImage());
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        TitleLbl.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        TitleLbl.setForeground(new java.awt.Color(255, 255, 255));
        TitleLbl.setText("User Information");
        getContentPane().add(TitleLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        NameLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        NameLbl.setForeground(new java.awt.Color(255, 255, 255));
        NameLbl.setText("Name:");
        getContentPane().add(NameLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, -1, -1));

        UsernameLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        UsernameLbl.setForeground(new java.awt.Color(255, 255, 255));
        UsernameLbl.setText("username:");
        getContentPane().add(UsernameLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, -1, -1));

        EmailLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        EmailLbl.setForeground(new java.awt.Color(255, 255, 255));
        EmailLbl.setText("Email:");
        EmailLbl.setToolTipText("");
        getContentPane().add(EmailLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, -1, -1));

        PhoneLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PhoneLbl.setForeground(new java.awt.Color(255, 255, 255));
        PhoneLbl.setText("Phone:");
        getContentPane().add(PhoneLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 230, -1, -1));

        LevelLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        LevelLbl.setForeground(new java.awt.Color(255, 255, 255));
        LevelLbl.setText("Permissions Level:");
        getContentPane().add(LevelLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 50, -1, -1));

        StatusLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        StatusLbl.setForeground(new java.awt.Color(255, 255, 255));
        StatusLbl.setText("Status:");
        getContentPane().add(StatusLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 110, -1, -1));

        RegisterByLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        RegisterByLbl.setForeground(new java.awt.Color(255, 255, 255));
        RegisterByLbl.setText("Register By:");
        getContentPane().add(RegisterByLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 170, -1, -1));

        NameTxt.setBackground(new java.awt.Color(153, 153, 255));
        NameTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        NameTxt.setForeground(new java.awt.Color(255, 255, 255));
        NameTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        NameTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(NameTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 210, -1));

        UsernameTxt.setBackground(new java.awt.Color(153, 153, 255));
        UsernameTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        UsernameTxt.setForeground(new java.awt.Color(255, 255, 255));
        UsernameTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        UsernameTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(UsernameTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, 210, -1));

        EmailTxt.setBackground(new java.awt.Color(153, 153, 255));
        EmailTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        EmailTxt.setForeground(new java.awt.Color(255, 255, 255));
        EmailTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        EmailTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(EmailTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 210, -1));

        PhoneTxt.setBackground(new java.awt.Color(153, 153, 255));
        PhoneTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        PhoneTxt.setForeground(new java.awt.Color(255, 255, 255));
        PhoneTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        PhoneTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(PhoneTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 250, 210, -1));

        RegisterByTxt.setBackground(new java.awt.Color(153, 153, 255));
        RegisterByTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        RegisterByTxt.setForeground(new java.awt.Color(255, 255, 255));
        RegisterByTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        RegisterByTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        RegisterByTxt.setEnabled(false);
        getContentPane().add(RegisterByTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 190, 210, -1));

        LevelsCmb.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "admin", "capturer", "tech" }));
        getContentPane().add(LevelsCmb, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 70, 210, -1));

        StatusCmb.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "active", "disable" }));
        getContentPane().add(StatusCmb, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 130, 210, -1));

        UpdateButton.setBackground(new java.awt.Color(153, 153, 255));
        UpdateButton.setFont(new java.awt.Font("Arial Narrow", 0, 18)); // NOI18N
        UpdateButton.setForeground(new java.awt.Color(255, 255, 255));
        UpdateButton.setText("Update User");
        UpdateButton.setBorder(null);
        UpdateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UpdateButtonActionPerformed(evt);
            }
        });
        getContentPane().add(UpdateButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 250, 210, 35));

        RestartPassLbl.setBackground(new java.awt.Color(153, 153, 255));
        RestartPassLbl.setFont(new java.awt.Font("Arial Narrow", 0, 18)); // NOI18N
        RestartPassLbl.setForeground(new java.awt.Color(255, 255, 255));
        RestartPassLbl.setText("Restart Pssword");
        RestartPassLbl.setBorder(null);
        RestartPassLbl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RestartPassLblActionPerformed(evt);
            }
        });
        getContentPane().add(RestartPassLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 300, 210, 35));

        CopyrightLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        CopyrightLbl.setText("Copyright © Danny Sequeira");
        getContentPane().add(CopyrightLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 390, -1, -1));
        getContentPane().add(BackgroundLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 630, 450));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void UpdateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UpdateButtonActionPerformed
        
        int level_cmb, status_cmb, validation = 0;
        String name, mail, phone, username, pass, levels_string = "", status_string = "";
        
        // Get data
        mail = EmailTxt.getText().trim();
        username = UsernameTxt.getText().trim();
        name = NameTxt.getText().trim();
        phone = PhoneTxt.getText().trim();
        
        level_cmb = LevelsCmb.getSelectedIndex() + 1;
        status_cmb = StatusCmb.getSelectedIndex() + 1;
        
        // Validation that there are not empty spaces
        if (mail.equals("")) {
            EmailTxt.setBackground(Color.red);
            validation ++;
        } 
        if (username.equals("")) {
            UsernameTxt.setBackground(Color.red);
            validation ++;
        }
        if (name.equals("")) {
            NameTxt.setBackground(Color.red);
            validation ++;
        } 
        if (phone.equals("")) {
            PhoneTxt.setBackground(Color.red);
            validation ++;
        }
        
        if (validation == 0) {
            if (level_cmb == 1) {
                levels_string = "admin";
            } else if (level_cmb == 2) {
                levels_string = "capturer";
            } else {
                levels_string = "tech";
            }
            
            if (status_cmb == 1) {
                status_string = "active";
            } else {
                status_string = "disable";
            }
            
            
            try {
                
                Connection cn = Conection.conect();
                PreparedStatement pst = cn.prepareStatement(
                        "select username from users where username = '" + username + "' and not id = '" + ID + "'"
                );
                
                ResultSet rst = pst.executeQuery();
                
                if (rst.next()) {
                    UsernameTxt.setBackground(Color.red);
                    JOptionPane.showMessageDialog(null, "Username is not available");
                    cn.close();
                } else {
                    Connection cn2 = Conection.conect();
                    PreparedStatement pst2 = cn2.prepareStatement(
                            "update users set name = ?, email = ?, phone = ?, username = ?, level = ?, status = ? "
                                    + "where id = '" + ID + "'"
                    );
                    
                    pst2.setString(1, name);
                    pst2.setString(2, mail);
                    pst2.setString(3, phone);
                    pst2.setString(4, username);
                    pst2.setString(5, levels_string);
                    pst2.setString(6, status_string);
                    
                    pst2.executeUpdate();
                    
                    cn2.close();
                    
                    JOptionPane.showMessageDialog(null, "Updated succesfully");
                
                }
                
                cn.close();
                
                
            } catch (SQLException e) {
                System.err.println("Error while updating the user!: " + e);
                JOptionPane.showMessageDialog(null, "Contact the admin");
            }
            
        } else {
            JOptionPane.showMessageDialog(null, "Error!! There are empty spaces!");
        }
        
    }//GEN-LAST:event_UpdateButtonActionPerformed

    private void RestartPassLblActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RestartPassLblActionPerformed
        PasswordModify pass = new PasswordModify();
        pass.setVisible(true);
    }//GEN-LAST:event_RestartPassLblActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            new UserInfo().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel BackgroundLbl;
    private javax.swing.JLabel CopyrightLbl;
    private javax.swing.JLabel EmailLbl;
    private javax.swing.JTextField EmailTxt;
    private javax.swing.JLabel LevelLbl;
    private javax.swing.JComboBox LevelsCmb;
    private javax.swing.JLabel NameLbl;
    private javax.swing.JTextField NameTxt;
    private javax.swing.JLabel PhoneLbl;
    private javax.swing.JTextField PhoneTxt;
    private javax.swing.JLabel RegisterByLbl;
    private javax.swing.JTextField RegisterByTxt;
    private javax.swing.JButton RestartPassLbl;
    private javax.swing.JComboBox StatusCmb;
    private javax.swing.JLabel StatusLbl;
    private javax.swing.JLabel TitleLbl;
    private javax.swing.JButton UpdateButton;
    private javax.swing.JLabel UsernameLbl;
    private javax.swing.JTextField UsernameTxt;
    // End of variables declaration//GEN-END:variables
}
