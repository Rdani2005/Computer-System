package windows;

import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import java.sql.*;
import clases.Conection;

import javax.swing.WindowConstants;

public class Administrator extends javax.swing.JFrame {

    String user, name;
    public static int user_session;
    
    public Administrator() {
        initComponents();
        
        user = Login.user;
        user_session = 1;
        
        setTitle("Admin - session owner: " + user);
        setSize(650, 430);
        setLocationRelativeTo(null);
        setResizable(false);
        // Background image configuration
        ImageIcon wallpaper = new ImageIcon("src/images/wallpaperPrincipal.jpg");
        Icon icon = new ImageIcon(
                wallpaper.getImage().getScaledInstance(
                        BackgroundLbl.getWidth(),
                        BackgroundLbl.getHeight(),
                        Image.SCALE_DEFAULT
                )
        );
        
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        
        BackgroundLbl.setIcon(icon);
        this.repaint();
        
        try {
            Connection cn = Conection.conect();
            PreparedStatement pst = cn.prepareStatement(
                    "select name from users where username = '" + user + "'"
            );
            
            ResultSet rs = pst.executeQuery();
            
            
            if (rs.next()) {
                name = rs.getString("name");
                UsernameLbl.setText(name);
            }
            
        } catch (Exception e) {
            System.err.println("Error!! " + e);
        }
    }
    
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/icon.png"));
        return retValue;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        UsernameLbl = new javax.swing.JLabel();
        RegisterUserButton5 = new javax.swing.JButton();
        CheckUsersButton = new javax.swing.JButton();
        CreativityButton = new javax.swing.JButton();
        CapturistButton = new javax.swing.JButton();
        TechButton = new javax.swing.JButton();
        AboutButton = new javax.swing.JButton();
        AddUserLbl = new javax.swing.JLabel();
        CheckUsersLbl = new javax.swing.JLabel();
        CreativityLbl = new javax.swing.JLabel();
        CapturistLbl = new javax.swing.JLabel();
        TechLbel = new javax.swing.JLabel();
        AboutLbl = new javax.swing.JLabel();
        CopyrightLbl = new javax.swing.JLabel();
        BackgroundLbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getIconImage());
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        UsernameLbl.setFont(new java.awt.Font("Arial", 1, 20)); // NOI18N
        UsernameLbl.setForeground(new java.awt.Color(255, 255, 255));
        UsernameLbl.setText("jLabel1");
        getContentPane().add(UsernameLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        RegisterUserButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/addUser.png"))); // NOI18N
        RegisterUserButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RegisterUserButton5ActionPerformed(evt);
            }
        });
        getContentPane().add(RegisterUserButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 70, 120, 120));

        CheckUsersButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/informationuser.png"))); // NOI18N
        CheckUsersButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckUsersButtonActionPerformed(evt);
            }
        });
        getContentPane().add(CheckUsersButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 70, 120, 120));

        CreativityButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/creatividad.png"))); // NOI18N
        getContentPane().add(CreativityButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 70, 120, 120));

        CapturistButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/capturista.png"))); // NOI18N
        CapturistButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CapturistButtonActionPerformed(evt);
            }
        });
        getContentPane().add(CapturistButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 240, 120, 120));

        TechButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tecnico.png"))); // NOI18N
        getContentPane().add(TechButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 240, 120, 120));

        AboutButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/editar.png"))); // NOI18N
        getContentPane().add(AboutButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 240, 120, 120));

        AddUserLbl.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        AddUserLbl.setForeground(new java.awt.Color(255, 255, 255));
        AddUserLbl.setText("Add Users");
        getContentPane().add(AddUserLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 190, -1, -1));

        CheckUsersLbl.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        CheckUsersLbl.setForeground(new java.awt.Color(255, 255, 255));
        CheckUsersLbl.setText("Check Users");
        getContentPane().add(CheckUsersLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 190, -1, -1));

        CreativityLbl.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        CreativityLbl.setForeground(new java.awt.Color(255, 255, 255));
        CreativityLbl.setText("Cretivity");
        getContentPane().add(CreativityLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 190, -1, -1));

        CapturistLbl.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        CapturistLbl.setForeground(new java.awt.Color(255, 255, 255));
        CapturistLbl.setText("Capturist View");
        getContentPane().add(CapturistLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 360, -1, -1));

        TechLbel.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        TechLbel.setForeground(new java.awt.Color(255, 255, 255));
        TechLbel.setText("Tech View");
        getContentPane().add(TechLbel, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 360, -1, -1));

        AboutLbl.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        AboutLbl.setForeground(new java.awt.Color(255, 255, 255));
        AboutLbl.setText("About");
        getContentPane().add(AboutLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 360, -1, -1));

        CopyrightLbl.setText("Copyright © Danny Sequeira");
        getContentPane().add(CopyrightLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 380, -1, -1));
        getContentPane().add(BackgroundLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 650, 430));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void RegisterUserButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RegisterUserButton5ActionPerformed
        AddUsersForm addUsers = new AddUsersForm();
        addUsers.setVisible(true);
    }//GEN-LAST:event_RegisterUserButton5ActionPerformed

    private void CheckUsersButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckUsersButtonActionPerformed
        ManageUsers users = new ManageUsers();
        users.setVisible(true);
    }//GEN-LAST:event_CheckUsersButtonActionPerformed

    private void CapturistButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CapturistButtonActionPerformed
        new Capturer().setVisible(true);
    }//GEN-LAST:event_CapturistButtonActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            new Administrator().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AboutButton;
    private javax.swing.JLabel AboutLbl;
    private javax.swing.JLabel AddUserLbl;
    private javax.swing.JLabel BackgroundLbl;
    private javax.swing.JButton CapturistButton;
    private javax.swing.JLabel CapturistLbl;
    private javax.swing.JButton CheckUsersButton;
    private javax.swing.JLabel CheckUsersLbl;
    private javax.swing.JLabel CopyrightLbl;
    private javax.swing.JButton CreativityButton;
    private javax.swing.JLabel CreativityLbl;
    private javax.swing.JButton RegisterUserButton5;
    private javax.swing.JButton TechButton;
    private javax.swing.JLabel TechLbel;
    private javax.swing.JLabel UsernameLbl;
    // End of variables declaration//GEN-END:variables
}
