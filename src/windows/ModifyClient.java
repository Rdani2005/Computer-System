package windows;

import java.awt.Image;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.HeadlessException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import java.sql.*;
import clases.Conection;

import com.itextpdf.text.Font;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.DocumentException;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ModifyClient extends javax.swing.JFrame {
    
    String user, clientUpdate;
    int ID_Update;
    public static int ID_computer;
    
    DefaultTableModel model = new DefaultTableModel();
    
    public ModifyClient() {
        initComponents();
        user = Login.user;
        ID_Update = ManageClients.Id_Client_Update;
        clientUpdate = ManageClients.clientUpdate;
        
        setSize(630, 450);
        setResizable(false);
        setLocationRelativeTo(null);
        setTitle("User information " + clientUpdate + " - Session Owner: " + user);

        // We don´t need the program to end
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        // Background image configuration
        ImageIcon wallpaper = new ImageIcon("src/images/wallpaperPrincipal.jpg");
        Icon icon = new ImageIcon(
                wallpaper.getImage().getScaledInstance(
                        BackgroundLbl.getWidth(),
                        BackgroundLbl.getHeight(),
                        Image.SCALE_DEFAULT
                )
        );
        BackgroundLbl.setIcon(icon);
        this.repaint();
        
        TitleLbl.setText("User Information: " + clientUpdate);
        
        UpdateData();

        try {
            Connection cn = Conection.conect();
            PreparedStatement pst = cn.prepareStatement(
                    "select id, type, brand, status from computers where id_client = '" + ID_Update + "'"
            );
            
            ResultSet rs = pst.executeQuery();

            ComputersTable = new JTable(model);
            ComputersTableScroll.setViewportView(ComputersTable);
            
            model.addColumn("ID");
            model.addColumn("Type");
            model.addColumn("Brand");
            model.addColumn("Status");
            
            while(rs.next()) {
                Object[] row = new Object[4];
                
                for (int i = 0; i < 4; i++) {
                    row[i] = rs.getObject(i + 1);
                }
                model.addRow(row);   
            }
            cn.close();
        } catch (SQLException e) {
            System.err.print("Error conecting to the DB on Change user Information Form!!! " + e);
            JOptionPane.showMessageDialog(null, "Error! Contact the admin for more information");
        }
                
        
    }
    
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/informationuser.png"));
        return retValue;
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TitleLbl = new javax.swing.JLabel();
        NameTxt = new javax.swing.JTextField();
        NameLbl = new javax.swing.JLabel();
        EmailTxt = new javax.swing.JTextField();
        EmailLbl = new javax.swing.JLabel();
        PhoneTxt = new javax.swing.JTextField();
        PhoneLbl = new javax.swing.JLabel();
        AddressTxt = new javax.swing.JTextField();
        AddressLbl = new javax.swing.JLabel();
        LastTxt = new javax.swing.JTextField();
        LastLbl = new javax.swing.JLabel();
        PrintButton = new javax.swing.JButton();
        UpdateButton = new javax.swing.JButton();
        AddComputerButton = new javax.swing.JButton();
        ComputersTableScroll = new javax.swing.JScrollPane();
        ComputersTable = new javax.swing.JTable();
        BackgroundLbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getIconImage());
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        TitleLbl.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        TitleLbl.setForeground(new java.awt.Color(255, 255, 255));
        TitleLbl.setText("Client Information");
        getContentPane().add(TitleLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        NameTxt.setBackground(new java.awt.Color(153, 153, 255));
        NameTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        NameTxt.setForeground(new java.awt.Color(255, 255, 255));
        NameTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        NameTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(NameTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 210, -1));

        NameLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        NameLbl.setForeground(new java.awt.Color(255, 255, 255));
        NameLbl.setText("Name:");
        getContentPane().add(NameLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, -1, -1));

        EmailTxt.setBackground(new java.awt.Color(153, 153, 255));
        EmailTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        EmailTxt.setForeground(new java.awt.Color(255, 255, 255));
        EmailTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        EmailTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(EmailTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, 210, -1));

        EmailLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        EmailLbl.setForeground(new java.awt.Color(255, 255, 255));
        EmailLbl.setText("Email:");
        getContentPane().add(EmailLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, -1, -1));

        PhoneTxt.setBackground(new java.awt.Color(153, 153, 255));
        PhoneTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        PhoneTxt.setForeground(new java.awt.Color(255, 255, 255));
        PhoneTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        PhoneTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(PhoneTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 210, -1));

        PhoneLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PhoneLbl.setForeground(new java.awt.Color(255, 255, 255));
        PhoneLbl.setText("Phone:");
        getContentPane().add(PhoneLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, -1, -1));

        AddressTxt.setBackground(new java.awt.Color(153, 153, 255));
        AddressTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        AddressTxt.setForeground(new java.awt.Color(255, 255, 255));
        AddressTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        AddressTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(AddressTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 250, 210, -1));

        AddressLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        AddressLbl.setForeground(new java.awt.Color(255, 255, 255));
        AddressLbl.setText("Address:");
        getContentPane().add(AddressLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 230, -1, -1));

        LastTxt.setEditable(false);
        LastTxt.setBackground(new java.awt.Color(153, 153, 255));
        LastTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        LastTxt.setForeground(new java.awt.Color(255, 255, 255));
        LastTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        LastTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(LastTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 310, 210, -1));

        LastLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        LastLbl.setForeground(new java.awt.Color(255, 255, 255));
        LastLbl.setText("Last Modified:");
        getContentPane().add(LastLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 290, -1, -1));

        PrintButton.setBackground(new java.awt.Color(204, 204, 204));
        PrintButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/impresora.png"))); // NOI18N
        getContentPane().add(PrintButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 270, 120, 120));

        UpdateButton.setBackground(new java.awt.Color(153, 153, 255));
        UpdateButton.setFont(new java.awt.Font("Arial Narrow", 0, 18)); // NOI18N
        UpdateButton.setForeground(new java.awt.Color(255, 255, 255));
        UpdateButton.setText("Update Client");
        UpdateButton.setBorder(null);
        UpdateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UpdateButtonActionPerformed(evt);
            }
        });
        getContentPane().add(UpdateButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 355, 210, 35));

        AddComputerButton.setBackground(new java.awt.Color(153, 153, 255));
        AddComputerButton.setFont(new java.awt.Font("Arial Narrow", 0, 18)); // NOI18N
        AddComputerButton.setForeground(new java.awt.Color(255, 255, 255));
        AddComputerButton.setText("Add Computer");
        AddComputerButton.setBorder(null);
        AddComputerButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddComputerButtonActionPerformed(evt);
            }
        });
        getContentPane().add(AddComputerButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 300, 210, 35));

        ComputersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        ComputersTableScroll.setViewportView(ComputersTable);

        getContentPane().add(ComputersTableScroll, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 40, 390, 220));
        getContentPane().add(BackgroundLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 630, 450));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void UpdateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UpdateButtonActionPerformed
        int validation = 0;
        String name, email, phone, address;
        
        name = NameTxt.getText().trim();
        email = EmailTxt.getText().trim();
        phone = PhoneTxt.getText().trim();
        address = AddressTxt.getText().trim();

        // Validation that there are not empty spaces
        if (email.equals("")) {
            EmailTxt.setBackground(Color.red);
            validation++;
        }
        if (name.equals("")) {
            NameTxt.setBackground(Color.red);
            validation++;
        }
        if (phone.equals("")) {
            PhoneTxt.setBackground(Color.red);
            validation++;
        }
        if (address.equals("")) {
            AddressTxt.setBackground(Color.red);
            validation++;
        }
        
        if (validation == 0) {
            
            try {
                Connection cn = Conection.conect();
                PreparedStatement pst = cn.prepareStatement(
                        "select name from clients where name = '" + name + "' and not id = '" + ID_Update + "'"
                );
                
                ResultSet rst = pst.executeQuery();
                
                if (rst.next()) {
                    JOptionPane.showMessageDialog(null, "There is another client with that name");
                    NameTxt.setBackground(Color.red);
                } else {
                    Connection cn2 = Conection.conect();
                    PreparedStatement pst2 = cn2.prepareStatement(
                            "update clients set name = ?, email = ?, phone = ?, adress = ?, last_modified = ? "
                            + "where id = '" + ID_Update + "'"
                    );
                    
                    pst2.setString(1, name);
                    pst2.setString(2, email);
                    pst2.setString(3, phone);
                    pst2.setString(4, address);
                    pst2.setString(5, user);
                    pst2.executeUpdate();
                    
                    cn2.close();
                    
                    JOptionPane.showMessageDialog(null, "Updated succesfully");
                    
                    UpdateData();
                }
                
                cn.close();
                
            } catch (SQLException | HeadlessException e) {
                JOptionPane.showMessageDialog(null, "Conection to the DB Failed. Call the admin!");
                System.err.println("Error updating the client! " + e);
            }
            
            
            
        } else {
            JOptionPane.showMessageDialog(null, "Fill all the inputs");
        }

    }//GEN-LAST:event_UpdateButtonActionPerformed

    private void AddComputerButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddComputerButtonActionPerformed
        new AddComputer().setVisible(true);
    }//GEN-LAST:event_AddComputerButtonActionPerformed
    
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            new ModifyClient().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AddComputerButton;
    private javax.swing.JLabel AddressLbl;
    private javax.swing.JTextField AddressTxt;
    private javax.swing.JLabel BackgroundLbl;
    private javax.swing.JTable ComputersTable;
    private javax.swing.JScrollPane ComputersTableScroll;
    private javax.swing.JLabel EmailLbl;
    private javax.swing.JTextField EmailTxt;
    private javax.swing.JLabel LastLbl;
    private javax.swing.JTextField LastTxt;
    private javax.swing.JLabel NameLbl;
    private javax.swing.JTextField NameTxt;
    private javax.swing.JLabel PhoneLbl;
    private javax.swing.JTextField PhoneTxt;
    private javax.swing.JButton PrintButton;
    private javax.swing.JLabel TitleLbl;
    private javax.swing.JButton UpdateButton;
    // End of variables declaration//GEN-END:variables
    
    private void setTextBoxes(String name, String email, String phone, String address, String last_modified) {
        NameTxt.setText(name);
        EmailTxt.setText(email);
        PhoneTxt.setText(phone);
        AddressTxt.setText(address);
        LastTxt.setText(last_modified);
    }
    
    private void UpdateData() {
        try {
            Connection cn = Conection.conect();
            
            PreparedStatement pst = cn.prepareStatement(
                    "select * from clients where id = '" + ID_Update + "'"
            );
            
            ResultSet rs = pst.executeQuery();
            
            if (rs.next()) {
                setTextBoxes(rs.getString("name"), rs.getString("email"), rs.getString("phone"), rs.getString("adress"), rs.getString("last_modified"));
            }
            cn.close();
        } catch (SQLException e) {
            System.err.print("Error while connecting to the database on to fullfill the textboxs!! " + e);
            JOptionPane.showMessageDialog(null, "Error! Contact the administrator");
        }
        
    }
}
