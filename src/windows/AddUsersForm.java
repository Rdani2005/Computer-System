package windows;

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import java.sql.*;
import clases.Conection;
import java.awt.HeadlessException;


public class AddUsersForm extends javax.swing.JFrame {

    String user;

    public AddUsersForm() {

        // Window configs
        initComponents();
        setSize(630, 350);
        setResizable(false);
        setLocationRelativeTo(null);
        // Background image configuration
        ImageIcon wallpaper = new ImageIcon("src/images/wallpaperPrincipal.jpg");
        Icon icon = new ImageIcon(
                wallpaper.getImage().getScaledInstance(
                        BackgroundLbl.getWidth(),
                        BackgroundLbl.getHeight(),
                        Image.SCALE_DEFAULT
                )
        );
        BackgroundLbl.setIcon(icon);
        this.repaint();

        // Logical configs
        user = Login.user;
        setTitle("Register new user: - Owner Session: " + user);
        // We don´t need the program to end
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/addUser.png"));
        return retValue;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TitleLbl = new javax.swing.JLabel();
        NameLbl = new javax.swing.JLabel();
        NameTxt = new javax.swing.JTextField();
        EmailLbl = new javax.swing.JLabel();
        EmailTxt = new javax.swing.JTextField();
        PhoneLbl = new javax.swing.JLabel();
        PhoneTxt = new javax.swing.JTextField();
        PermitionsLbl = new javax.swing.JLabel();
        LevelsCmb = new javax.swing.JComboBox();
        usernameLbl = new javax.swing.JLabel();
        UsernameTxt = new javax.swing.JTextField();
        PasswordLbl = new javax.swing.JLabel();
        PasswordTxt = new javax.swing.JPasswordField();
        AddUserButton = new javax.swing.JButton();
        CopyrightLbl = new javax.swing.JLabel();
        BackgroundLbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getIconImage());
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        TitleLbl.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        TitleLbl.setForeground(new java.awt.Color(255, 255, 255));
        TitleLbl.setText("User Register");
        getContentPane().add(TitleLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 10, -1, -1));

        NameLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        NameLbl.setForeground(new java.awt.Color(255, 255, 255));
        NameLbl.setText("Name:");
        getContentPane().add(NameLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, -1, -1));

        NameTxt.setBackground(new java.awt.Color(153, 153, 255));
        NameTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        NameTxt.setForeground(new java.awt.Color(255, 255, 255));
        NameTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        NameTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(NameTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 210, -1));

        EmailLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        EmailLbl.setForeground(new java.awt.Color(255, 255, 255));
        EmailLbl.setText("email:");
        getContentPane().add(EmailLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, -1, -1));

        EmailTxt.setBackground(new java.awt.Color(153, 153, 255));
        EmailTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        EmailTxt.setForeground(new java.awt.Color(255, 255, 255));
        EmailTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        EmailTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(EmailTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, 210, -1));

        PhoneLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PhoneLbl.setForeground(new java.awt.Color(255, 255, 255));
        PhoneLbl.setText("Phone:");
        getContentPane().add(PhoneLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, -1, -1));
        PhoneLbl.getAccessibleContext().setAccessibleName("Phone");

        PhoneTxt.setBackground(new java.awt.Color(153, 153, 255));
        PhoneTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        PhoneTxt.setForeground(new java.awt.Color(255, 255, 255));
        PhoneTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        PhoneTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(PhoneTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 210, -1));

        PermitionsLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PermitionsLbl.setForeground(new java.awt.Color(255, 255, 255));
        PermitionsLbl.setText("Permitions:");
        getContentPane().add(PermitionsLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 230, -1, -1));

        LevelsCmb.setBackground(new java.awt.Color(153, 153, 255));
        LevelsCmb.setForeground(new java.awt.Color(255, 255, 255));
        LevelsCmb.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "admin", "capturer", "tech" }));
        getContentPane().add(LevelsCmb, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 250, 210, -1));

        usernameLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        usernameLbl.setForeground(new java.awt.Color(255, 255, 255));
        usernameLbl.setText("Username:");
        getContentPane().add(usernameLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 50, -1, -1));

        UsernameTxt.setBackground(new java.awt.Color(153, 153, 255));
        UsernameTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        UsernameTxt.setForeground(new java.awt.Color(255, 255, 255));
        UsernameTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        UsernameTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(UsernameTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 70, 210, -1));

        PasswordLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PasswordLbl.setForeground(new java.awt.Color(255, 255, 255));
        PasswordLbl.setText("Password:");
        getContentPane().add(PasswordLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 110, -1, -1));

        PasswordTxt.setBackground(new java.awt.Color(153, 153, 255));
        PasswordTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        PasswordTxt.setForeground(new java.awt.Color(255, 255, 255));
        PasswordTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        PasswordTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(PasswordTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 130, 210, -1));

        AddUserButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/add.png"))); // NOI18N
        AddUserButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddUserButtonActionPerformed(evt);
            }
        });
        getContentPane().add(AddUserButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 170, 120, 120));

        CopyrightLbl.setText("Copyright © Danny Sequeira");
        getContentPane().add(CopyrightLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 300, -1, -1));
        getContentPane().add(BackgroundLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 630, 350));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void AddUserButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddUserButtonActionPerformed
        int levels_cmd, validation = 0;
        String name, username, mail, phone, password, string_levels;
        // Getting Dat from the user.
        name = NameTxt.getText().trim();
        username = UsernameTxt.getText().trim();
        password = PasswordTxt.getText().trim();
        mail = EmailTxt.getText().trim();
        phone = PhoneTxt.getText().trim();

        if (mail.equals("")) {
            EmailTxt.setBackground(Color.red);
            validation++;
        }

        if (name.equals("")) {
            NameTxt.setBackground(Color.red);
            validation++;
        }

        if (username.equals("")) {
            UsernameTxt.setBackground(Color.red);
            validation++;
        }

        if (phone.equals("")) {
            PhoneTxt.setBackground(Color.red);
            validation++;
        }

        if (password.equals("")) {
            PasswordTxt.setBackground(Color.red);
            validation++;
        }

        // Getting data from the combobox
        levels_cmd = LevelsCmb.getSelectedIndex() + 1;

        if (levels_cmd == 1) {
            string_levels = "admin";
        } else if (levels_cmd == 2) {
            string_levels = "capturer";
        } else {
            string_levels = "tech";
        }

        try {
            Connection cn = Conection.conect();
            PreparedStatement pst = cn.prepareStatement(
                    "select username from users where username = '" + username + "'"
            );

            ResultSet rs = pst.executeQuery();

            if (rs.next()) {
                UsernameTxt.setBackground(Color.red);
                JOptionPane.showMessageDialog(null, "Username is not available");
                cn.close();
            } else {
                cn.close();

                if (validation == 0) {
                    try {
                        Connection cn2 = Conection.conect();
                        PreparedStatement pst2 = cn2.prepareStatement(
                                "insert into users values (?,?,?,?,?,?,?,?,?)"
                        );
                        // Adding data to the database
                        pst2.setInt(1, 0);
                        pst2.setString(2, name);
                        pst2.setString(3, mail);
                        pst2.setString(4, phone);
                        pst2.setString(5, username);
                        pst2.setString(6, password);
                        pst2.setString(7, string_levels);
                        pst2.setString(8, "active");
                        pst2.setString(9, user);
                        
                        pst2.executeUpdate();
                        cn2.close();
                        
                        cleanText();
                        EmailTxt.setBackground(Color.GREEN);
                        UsernameTxt.setBackground(Color.GREEN);
                        NameTxt.setBackground(Color.GREEN);
                        PhoneTxt.setBackground(Color.GREEN);
                        
                        
                        
                        JOptionPane.showMessageDialog(null, "User added successfully!");
                        
                        this.dispose();
                        
                    } catch (SQLException | HeadlessException e) {
                        System.err.println("Error adding the user! " + e);
                        JOptionPane.showMessageDialog(null, "Database Error!!!");
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "There are empty spaces");
                }

            }

        } catch (SQLException | HeadlessException e) {
            System.err.println("Error validating username!! " + e);
        }
    }//GEN-LAST:event_AddUserButtonActionPerformed
    
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            new AddUsersForm().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AddUserButton;
    private javax.swing.JLabel BackgroundLbl;
    private javax.swing.JLabel CopyrightLbl;
    private javax.swing.JLabel EmailLbl;
    private javax.swing.JTextField EmailTxt;
    private javax.swing.JComboBox LevelsCmb;
    private javax.swing.JLabel NameLbl;
    private javax.swing.JTextField NameTxt;
    private javax.swing.JLabel PasswordLbl;
    private javax.swing.JPasswordField PasswordTxt;
    private javax.swing.JLabel PermitionsLbl;
    private javax.swing.JLabel PhoneLbl;
    private javax.swing.JTextField PhoneTxt;
    private javax.swing.JLabel TitleLbl;
    private javax.swing.JTextField UsernameTxt;
    private javax.swing.JLabel usernameLbl;
    // End of variables declaration//GEN-END:variables


    private void cleanText() {
        UsernameTxt.setText("");
        NameTxt.setText("");
        PasswordTxt.setText("");
        PhoneTxt.setText("");
        EmailTxt.setText("");
        
        LevelsCmb.setSelectedIndex(0);
    }


}
