package windows;

import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.WindowConstants;

import java.sql.*;
import clases.Conection;

import java.awt.Color;

import javax.swing.JOptionPane;

public class PasswordModify extends javax.swing.JFrame {

    String user, userUpdate;

    public PasswordModify() {
        initComponents();
        // Get data from other Forms
        user = Login.user;
        userUpdate = ManageUsers.userUpdate;

        setTitle("Restart Password from: " + userUpdate + " - Session Owner: " + user);
        setSize(360, 260);
        setResizable(false);
        setLocationRelativeTo(null);
        // We don´t need the program to end
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        // Background image configuration
        ImageIcon wallpaper = new ImageIcon("src/images/wallpaperPrincipal.jpg");
        Icon icon = new ImageIcon(
                wallpaper.getImage().getScaledInstance(
                        BackgroundLbl.getWidth(),
                        BackgroundLbl.getHeight(),
                        Image.SCALE_DEFAULT
                )
        );
        BackgroundLbl.setIcon(icon);
        this.repaint();
    }

    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/informationuser.png"));
        return retValue;
    }
    

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TitleLbl = new javax.swing.JLabel();
        PasswordLbl = new javax.swing.JLabel();
        ConfirmLbl = new javax.swing.JLabel();
        RestartPassLbl = new javax.swing.JButton();
        NewPassTxt = new javax.swing.JPasswordField();
        ConfirmTxt = new javax.swing.JPasswordField();
        BackgroundLbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getIconImage());
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        TitleLbl.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        TitleLbl.setForeground(new java.awt.Color(255, 255, 255));
        TitleLbl.setText("Restart Password");
        getContentPane().add(TitleLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 10, -1, -1));

        PasswordLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PasswordLbl.setForeground(new java.awt.Color(255, 255, 255));
        PasswordLbl.setText("New Password:");
        getContentPane().add(PasswordLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, -1, -1));

        ConfirmLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        ConfirmLbl.setForeground(new java.awt.Color(255, 255, 255));
        ConfirmLbl.setText("Confirm Password:");
        getContentPane().add(ConfirmLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, -1, -1));
        ConfirmLbl.getAccessibleContext().setAccessibleDescription("");

        RestartPassLbl.setBackground(new java.awt.Color(153, 153, 255));
        RestartPassLbl.setFont(new java.awt.Font("Arial Narrow", 0, 18)); // NOI18N
        RestartPassLbl.setForeground(new java.awt.Color(255, 255, 255));
        RestartPassLbl.setText("Restart Password");
        RestartPassLbl.setBorder(null);
        RestartPassLbl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RestartPassLblActionPerformed(evt);
            }
        });
        getContentPane().add(RestartPassLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, 210, 35));

        NewPassTxt.setBackground(new java.awt.Color(153, 153, 255));
        NewPassTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        NewPassTxt.setForeground(new java.awt.Color(255, 255, 255));
        NewPassTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        NewPassTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(NewPassTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 210, -1));

        ConfirmTxt.setBackground(new java.awt.Color(153, 153, 255));
        ConfirmTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        ConfirmTxt.setForeground(new java.awt.Color(255, 255, 255));
        ConfirmTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        ConfirmTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(ConfirmTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, 210, -1));
        getContentPane().add(BackgroundLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 360, 260));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void RestartPassLblActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RestartPassLblActionPerformed
        String password, confirmPassword;
        
        password = NewPassTxt.getText().trim();
        confirmPassword = ConfirmTxt.getText().trim();
        
        if (!password.equals("") && !confirmPassword.equals("")) {
            if (password.equals(confirmPassword)) {
                try {
                    Connection cn = Conection.conect();
                    PreparedStatement pst = cn.prepareStatement(
                            "update users set password = ? where username = '" + userUpdate + "'"
                    );
                    
                    pst.setString(1, password);
                    pst.executeUpdate();
                    
                    cn.close();
                    
                    LightTxt(Color.green);
                    JOptionPane.showMessageDialog(null, "Password Restart Successfully");
                    this.dispose();
                    
                } catch (SQLException e) {
                    System.err.println("Error modifying the password!!! " + e);
                    JOptionPane.showMessageDialog(null, "Error!! Call the Admin!");
                }
                
                
            } else {
                LightTxt(Color.red);
                JOptionPane.showMessageDialog(null, "The psswords don't match");
            }
            
            
        } else {
            LightTxt(Color.red);
            JOptionPane.showMessageDialog(null, "There are empty spaces");
        }
        
    }//GEN-LAST:event_RestartPassLblActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            new PasswordModify().setVisible(true);
        });
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel BackgroundLbl;
    private javax.swing.JLabel ConfirmLbl;
    private javax.swing.JPasswordField ConfirmTxt;
    private javax.swing.JPasswordField NewPassTxt;
    private javax.swing.JLabel PasswordLbl;
    private javax.swing.JButton RestartPassLbl;
    private javax.swing.JLabel TitleLbl;
    // End of variables declaration//GEN-END:variables

    private void LightTxt(Color color) {
        NewPassTxt.setBackground(color);
        ConfirmTxt.setBackground(color);
    }
}
