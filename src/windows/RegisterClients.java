package windows;

import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.WindowConstants;

import java.sql.*;
import clases.Conection;
import java.awt.Color;
import javax.swing.JOptionPane;



public class RegisterClients extends javax.swing.JFrame {

    String user;
    
    public RegisterClients() {
        initComponents();
        
        user = Login.user;
        
        setTitle("Add Client - Session Owner: " + user);
        setSize(530, 350);
        setResizable(false);
        setLocationRelativeTo(null);
        // We don´t need the program to end
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        // Background image configuration
        ImageIcon wallpaper = new ImageIcon("src/images/wallpaperPrincipal.jpg");
        Icon icon = new ImageIcon(
                wallpaper.getImage().getScaledInstance(
                        BackgroundLbl.getWidth(),
                        BackgroundLbl.getHeight(),
                        Image.SCALE_DEFAULT
                )
        );
        BackgroundLbl.setIcon(icon);
        this.repaint();    
    }
    
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/addUser.png"));
        return retValue;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TitleLbl = new javax.swing.JLabel();
        NameLbl = new javax.swing.JLabel();
        NameTxt = new javax.swing.JTextField();
        EmailLbl = new javax.swing.JLabel();
        EmailTxt = new javax.swing.JTextField();
        PhoneLbl = new javax.swing.JLabel();
        PhoneTxt = new javax.swing.JTextField();
        AddressLbl = new javax.swing.JLabel();
        AdressTxt = new javax.swing.JTextField();
        CopyrightLbl = new javax.swing.JLabel();
        AddUserButton = new javax.swing.JButton();
        BackgroundLbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getIconImage());
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        TitleLbl.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        TitleLbl.setForeground(new java.awt.Color(255, 255, 255));
        TitleLbl.setText("Clients Register");
        getContentPane().add(TitleLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 10, -1, -1));

        NameLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        NameLbl.setForeground(new java.awt.Color(255, 255, 255));
        NameLbl.setText("Name:");
        getContentPane().add(NameLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, -1, -1));

        NameTxt.setBackground(new java.awt.Color(153, 153, 255));
        NameTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        NameTxt.setForeground(new java.awt.Color(255, 255, 255));
        NameTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        NameTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(NameTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 210, -1));

        EmailLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        EmailLbl.setForeground(new java.awt.Color(255, 255, 255));
        EmailLbl.setText("Email");
        getContentPane().add(EmailLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, -1, -1));

        EmailTxt.setBackground(new java.awt.Color(153, 153, 255));
        EmailTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        EmailTxt.setForeground(new java.awt.Color(255, 255, 255));
        EmailTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        EmailTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(EmailTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, 210, -1));

        PhoneLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        PhoneLbl.setForeground(new java.awt.Color(255, 255, 255));
        PhoneLbl.setText("Phone:");
        getContentPane().add(PhoneLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, -1, -1));

        PhoneTxt.setBackground(new java.awt.Color(153, 153, 255));
        PhoneTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        PhoneTxt.setForeground(new java.awt.Color(255, 255, 255));
        PhoneTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        PhoneTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(PhoneTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 210, -1));

        AddressLbl.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        AddressLbl.setForeground(new java.awt.Color(255, 255, 255));
        AddressLbl.setText("Adress:");
        getContentPane().add(AddressLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 230, -1, -1));

        AdressTxt.setBackground(new java.awt.Color(153, 153, 255));
        AdressTxt.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        AdressTxt.setForeground(new java.awt.Color(255, 255, 255));
        AdressTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        AdressTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(AdressTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 250, 210, -1));

        CopyrightLbl.setText("Copyright © Danny Sequeira");
        getContentPane().add(CopyrightLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 300, -1, -1));

        AddUserButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/add.png"))); // NOI18N
        AddUserButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddUserButtonActionPerformed(evt);
            }
        });
        getContentPane().add(AddUserButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 100, 120, 120));
        getContentPane().add(BackgroundLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 530, 350));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void AddUserButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddUserButtonActionPerformed
        int validation = 0;
        String name, email, phone, address;
        
        name = NameTxt.getText().trim();
        email = EmailTxt.getText().trim();
        phone = PhoneTxt.getText().trim();
        address = AdressTxt.getText().trim();
        
        if (name.equals("")) {
            NameTxt.setBackground(Color.red);
            validation++;
        } 
        if (email.equals("")) {
            EmailTxt.setBackground(Color.red);
            validation++;
        } 
        if (phone.equals("")) {
            PhoneTxt.setBackground(Color.red);
            validation++;
        } 
        if (address.equals("")) {
            AdressTxt.setBackground(Color.red);
            validation++;
        }
        
        if (validation == 0) {
            
            try {
            
                Connection cn = Conection.conect();
                PreparedStatement pst = cn.prepareStatement(
                        "insert into clients values (?,?,?,?,?,?)"
                );
                
                pst.setInt(1, 0);
                pst.setString(2, name);
                pst.setString(3, email);
                pst.setString(4, phone);
                pst.setString(5, address);
                pst.setString(6, user);
                
                pst.executeUpdate();
                
                cn.close();
                
                CleanTxt();
                HighLightText(Color.GREEN);
                
                JOptionPane.showMessageDialog(null, "Client Added Successfully");
                this.dispose();
                
            } catch (SQLException e) {
                System.err.println("Error while adding the client on the Database: " + e);
                JOptionPane.showMessageDialog(null, "Contact the Admin");
            }
            
            
            
        } else {
            JOptionPane.showMessageDialog(null, "You should fill all the inputs");
        }
        
    }//GEN-LAST:event_AddUserButtonActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            new RegisterClients().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AddUserButton;
    private javax.swing.JLabel AddressLbl;
    private javax.swing.JTextField AdressTxt;
    private javax.swing.JLabel BackgroundLbl;
    private javax.swing.JLabel CopyrightLbl;
    private javax.swing.JLabel EmailLbl;
    private javax.swing.JTextField EmailTxt;
    private javax.swing.JLabel NameLbl;
    private javax.swing.JTextField NameTxt;
    private javax.swing.JLabel PhoneLbl;
    private javax.swing.JTextField PhoneTxt;
    private javax.swing.JLabel TitleLbl;
    // End of variables declaration//GEN-END:variables

    private void CleanTxt() {
        NameTxt.setText("");
        AdressTxt.setText("");
        EmailTxt.setText("");
        PhoneTxt.setText("");
    }
    
    private void HighLightText(Color color) {
        NameTxt.setBackground(color);
        AdressTxt.setBackground(color);
        EmailTxt.setBackground(color);
        PhoneTxt.setBackground(color);
    }

}
