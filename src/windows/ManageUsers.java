package windows;


import javax.swing.Icon;
import javax.swing.JTable;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

import java.sql.*;
import clases.Conection;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ManageUsers extends javax.swing.JFrame {

    String user;
    public static String userUpdate = "";
    DefaultTableModel model = new DefaultTableModel();
    
    public ManageUsers() {
        initComponents();
        
        // Window configs
        user = Login.user;
        setTitle("Registered users - session owner: " + user);
        setSize(630, 330);
        setResizable(false);
        setLocationRelativeTo(null);
        // We don´t need the program to end
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE); 
        
        // Background image configuration
        ImageIcon wallpaper = new ImageIcon("src/images/wallpaperPrincipal.jpg");
        Icon icon = new ImageIcon(
                wallpaper.getImage().getScaledInstance(
                        BackgroundLbl.getWidth(),
                        BackgroundLbl.getHeight(),
                        Image.SCALE_DEFAULT
                )
        );
        BackgroundLbl.setIcon(icon);
        this.repaint();
        
        
        try {
            
            Connection cn = Conection.conect();
            PreparedStatement pst = cn.prepareStatement(
                    "select id, name, username, level, status from users"
            );
            
            ResultSet rs = pst.executeQuery();

            UsersTable = new JTable(model);
            UsersTableScroll.setViewportView(UsersTable);
            
            model.addColumn(" ");
            model.addColumn("user");
            model.addColumn("username");
            model.addColumn("level");
            model.addColumn("status");
            
            while(rs.next()) {
                Object[] row = new Object[5];
                
                for (int i = 0; i < 5; i++) {
                    row[i] = rs.getObject(i + 1);
                }
                model.addRow(row);   
            }
            cn.close();
            
        } catch (SQLException e) {
            System.err.print("Error conecting to the DB on Manaage Users Form!!! " + e);
            JOptionPane.showMessageDialog(null, "Error! Contact the admin for more information");
        }
        
        
        UsersTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                int row_pointer = UsersTable.rowAtPoint(evt.getPoint());
                int column_pointer = 2;
                
                if (row_pointer > -1) {
                    userUpdate = (String) model.getValueAt(row_pointer, column_pointer);
                    UserInfo info = new UserInfo();
                    info.setVisible(true);
                }
            }
        
        });
        
    }

    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/informationuser.png"));
        return retValue;
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TitleLbl = new javax.swing.JLabel();
        UsersTableScroll = new javax.swing.JScrollPane();
        UsersTable = new javax.swing.JTable();
        CopyrightLbl = new javax.swing.JLabel();
        BackgroundLbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getIconImage());
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        TitleLbl.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        TitleLbl.setForeground(new java.awt.Color(255, 255, 255));
        TitleLbl.setText("Users on Database");
        getContentPane().add(TitleLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 10, -1, -1));

        UsersTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        UsersTableScroll.setViewportView(UsersTable);

        getContentPane().add(UsersTableScroll, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 70, 630, 180));

        CopyrightLbl.setText("Copyright © Danny Sequeira");
        getContentPane().add(CopyrightLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 280, -1, -1));
        getContentPane().add(BackgroundLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 630, 330));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ManageUsers.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ManageUsers.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ManageUsers.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ManageUsers.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ManageUsers().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel BackgroundLbl;
    private javax.swing.JLabel CopyrightLbl;
    private javax.swing.JLabel TitleLbl;
    private javax.swing.JTable UsersTable;
    private javax.swing.JScrollPane UsersTableScroll;
    // End of variables declaration//GEN-END:variables
}
