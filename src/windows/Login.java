package windows;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.HeadlessException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import java.sql.*;
import clases.Conection;

public class Login extends javax.swing.JFrame {

    public static String user = "";
    String pass = "";

    public Login() {
        initComponents();
        // Extras configs
        setSize(400, 550);
        setResizable(false);
        setTitle("Login");
        setLocationRelativeTo(null);

        // Background image configuration
        ImageIcon wallpaper = new ImageIcon("src/images/wallpaperPrincipal.jpg");
        Icon icon = new ImageIcon(
                wallpaper.getImage().getScaledInstance(
                        background_lbl.getWidth(),
                        background_lbl.getHeight(),
                        Image.SCALE_DEFAULT
                )
        );

        background_lbl.setIcon(icon);
        this.repaint();

        // Logo configs
        ImageIcon logo = new ImageIcon("src/images/DS.png");
        Icon logo_icon = new ImageIcon(
                logo.getImage().getScaledInstance(
                        Logo_lbl.getWidth(),
                        Logo_lbl.getHeight(),
                        Image.SCALE_DEFAULT
                )
        );

        Logo_lbl.setIcon(logo_icon);

        this.repaint();
    }

    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("images/icon.png"));
        return retValue;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Logo_lbl = new javax.swing.JLabel();
        UserTxt = new javax.swing.JTextField();
        PasswordTxt = new javax.swing.JPasswordField();
        NameLbl = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        LoginButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        background_lbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getIconImage());
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        getContentPane().add(Logo_lbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 10, 270, 270));

        UserTxt.setBackground(new java.awt.Color(153, 153, 255));
        UserTxt.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        UserTxt.setForeground(new java.awt.Color(255, 255, 255));
        UserTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        UserTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        UserTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UserTxtActionPerformed(evt);
            }
        });
        getContentPane().add(UserTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(95, 330, 210, -1));

        PasswordTxt.setBackground(new java.awt.Color(153, 153, 255));
        PasswordTxt.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        PasswordTxt.setForeground(new java.awt.Color(255, 255, 255));
        PasswordTxt.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        PasswordTxt.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        getContentPane().add(PasswordTxt, new org.netbeans.lib.awtextra.AbsoluteConstraints(95, 380, 210, -1));

        NameLbl.setText("UserName");
        getContentPane().add(NameLbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 300, -1, -1));

        jLabel2.setText("Password");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 360, -1, -1));

        LoginButton.setBackground(new java.awt.Color(153, 153, 255));
        LoginButton.setFont(new java.awt.Font("Arial Narrow", 1, 18)); // NOI18N
        LoginButton.setForeground(new java.awt.Color(255, 255, 255));
        LoginButton.setText("Login");
        LoginButton.setBorder(null);
        LoginButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LoginButtonActionPerformed(evt);
            }
        });
        getContentPane().add(LoginButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(95, 430, 210, 35));

        jLabel1.setText("© Copyright Danny Sequeira ©");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 500, -1, -1));
        getContentPane().add(background_lbl, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 550));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void UserTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UserTxtActionPerformed
        
    }//GEN-LAST:event_UserTxtActionPerformed

    private void LoginButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LoginButtonActionPerformed

        user = UserTxt.getText().trim();
        pass = PasswordTxt.getText().trim();

        if (!user.equals("") || !pass.equals("")) {
            try {
                Connection cn = Conection.conect();
                PreparedStatement pst = cn.prepareStatement(
                        "select level, status from users where username = '" + user
                        + "' and password = '" + pass + "'"
                );

                ResultSet rs = pst.executeQuery();

                if (rs.next()) {
                    
                    String level = rs.getString("level");
                    String status = rs.getString("status");
                    
                    if (level.equals("admin") && status.equalsIgnoreCase("active")) {
                        dispose();
                        new Administrator().setVisible(true);
                        
                    } else if (level.equals("capturer") && status.equalsIgnoreCase("active")) {
                        dispose();
                        new Capturer().setVisible(true);
                        
                    } else if (level.equals("tech") && status.equalsIgnoreCase("active")) {
                        dispose();
                        new Tech().setVisible(true);
                    } else {
                        JOptionPane.showMessageDialog(null, "The user is not active");
                    }
                    
                } else {
                    JOptionPane.showMessageDialog(null, "Password or username are incorrect!");
                    UserTxt.setText("");
                    PasswordTxt.setText("");
                }

            } catch (SQLException | HeadlessException e) {
                System.err.print("Error on login button. " + e);
                JOptionPane.showMessageDialog(null, "Program error! Call the Admin!");
            }

        } else {
            JOptionPane.showMessageDialog(null, "There are empty spaces!");
        }


    }//GEN-LAST:event_LoginButtonActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            new Login().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton LoginButton;
    private javax.swing.JLabel Logo_lbl;
    private javax.swing.JLabel NameLbl;
    private javax.swing.JPasswordField PasswordTxt;
    private javax.swing.JTextField UserTxt;
    private javax.swing.JLabel background_lbl;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
}
